# README #

Common components, utils, services

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* My email onixbed@gmail.com

The software is released under the Apache License version 2. (http://www.apache.org/licenses/LICENSE-2.0)

For used libraries see the NOTICE file.

# Релиз новой версии
mvn clean release:prepare release:perform
выпуск javadoc deploy--javadoc.bat
