package ru.maksimov.andrey.commons.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import ru.maksimov.andrey.commons.exception.BusinessException;

/**
 * Вспомогаткельный класс для даты
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class DateUtils {

	public static final String FULL_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss.SSS";
	public static final String BASE_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";
	public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String SHORT_DATE_FORMAT_USA = "yyyy-MM-dd";
	public static final String SHORT_DATE_FORMAT_RUSSIA = "dd.MM.yyyy";
	public static final String SIMPLE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String ISO_TIME_FORMAT = "HH:mm:ss";
	public static final String SHORT_TIME_FORMAT_RUSSIA = "HH:mm";

	/**
	 * Добавляет или вычитает указанное количество времени
	 * 
	 * @param date
	 *            дата
	 * @param field
	 *            еденица времени которую нужно добавить see
	 *            Calendar.DAY_OF_MONTH
	 * @param value
	 *            значение
	 * @return обновленная дата
	 */
	public static Date addDate(Date date, int field, int value) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(field, value);
		return calendar.getTime();
	}

	/**
	 * Преобразовать дату в строку
	 * 
	 * @param date
	 *            дата
	 * @param format
	 *            формат
	 * @return дата в заданном формате
	 */
	public static String formatDate(Date date, String format) {
		return formatDate(date, format, null);
	}

	/**
	 * Преобразовать дату в строку
	 * 
	 * @param date
	 *            дата
	 * @param format
	 *            формат
	 * @param zone
	 *            зона может быть null
	 * @return дата в заданном формате
	 */
	public static String formatDate(Date date, String format, TimeZone zone) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		if(zone != null) {
			sdf.setTimeZone(zone);
		}
		return sdf.format(date);
	}

	/**
	 * Преобразовать строку в дату
	 * 
	 * @param date
	 *            дата
	 * @param formats
	 *            форматы дат
	 * @return дата, null если нет нужного формата или список пустой
	 * @throws BusinessException
	 *             ошибка не один из шаблонов не подошел
	 */
	public static Date parseDate(String date, List<String> formats) throws BusinessException {
		ParseException error = new ParseException("Unable parse date format is Empty", 0);
		for (String format : formats) {
			try {
				return new SimpleDateFormat(format).parse(date);
			} catch (ParseException pe) {
				error = pe;
			}
		}
		throw new BusinessException("Unable parse date:" + error.getMessage(), error);
	}
}
