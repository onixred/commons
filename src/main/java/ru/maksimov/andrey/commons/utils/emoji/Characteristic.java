package ru.maksimov.andrey.commons.utils.emoji;

/**
 * Вспомогаткельный класс для emoji с характеристиками
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public enum Characteristic {

	Animal,

	Person,

	Face,

	Food,

	Plant,

	Machine,

	Sign
}
