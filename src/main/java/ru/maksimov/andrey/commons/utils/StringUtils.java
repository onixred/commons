package ru.maksimov.andrey.commons.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Вспомогаткельный класс для строки
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class StringUtils {

	/**
	 * Удалить все после "разделителя"
	 * 
	 * @param string
	 *            строка
	 * @param delimiter
	 *            разделитель
	 * @return подстрока если найден разделитель иначе вся строка
	 */
	public static String firstSubstring(String string, String delimiter) {
		if (string != null && !string.isEmpty()) {
			String[] arry = string.split(delimiter);
			if (arry.length > 0) {
				return arry[0];
			}
		}
		return string;
	}

	/**
	 * Сериализовать любой объект
	 * 
	 * @param object
	 *            объект
	 * @return строка
	 * @throws BusinessException
	 *             бизнес ошибка преобразования объекта
	 * @throws VerificationException
	 *             ошибра на этапе верификации объекта
	 */
	public static String serialize(Object object) throws VerificationException, BusinessException {
		String string = null;
		if (object == null) {
			throw new VerificationException("Unable serialize object. String is null");
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			string = mapper.writeValueAsString(object);
		} catch (JsonProcessingException jpe) {
			throw new BusinessException("Unable serialize object:" + jpe.getMessage(), jpe);
		}
		return string;
	}

	/**
	 * Десериализация любого объекта
	 * 
	 * @param string
	 *            строка
	 * @param typeReference
	 *            тип возрощаемого объекта
	 * @param <T>
	 *            темлейт для возврата
	 * @return объект
	 * 
	 * @throws BusinessException
	 *             бизнес ошибка преобразования объекта
	 * @throws VerificationException
	 *             ошибра на этапе верификации объекта
	 */
	public static <T> T deserialize(String string, TypeReference<T> typeReference)
			throws VerificationException, BusinessException {
		T object = null;
		if (string == null || string.isEmpty()) {
			throw new VerificationException("Unable deserialize string. String is empty");
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			object = mapper.readValue(string, typeReference);
		} catch (IOException e) {
			throw new BusinessException("Unable deserialize string:" + e.getMessage(), e);
		}
		return object;
	}
}
