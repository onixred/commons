package ru.maksimov.andrey.commons.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * Аспект для аннотации see Loggable
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@Component
@Aspect
public class LoggingAspect {

	@Around(value = "@within(ru.maksimov.andrey.commons.log.Loggable) || @annotation(ru.maksimov.andrey.commons.log.Loggable)")
	public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

		MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
		Method method = signature.getMethod();
		Loggable loggableMethod = method.getAnnotation(Loggable.class);
		
		Loggable loggableClass = proceedingJoinPoint.getTarget().getClass().getAnnotation(Loggable.class);
	
		if(loggableMethod == null && loggableMethod == null) {
			method = proceedingJoinPoint.getTarget().getClass().getMethod(method.getName(), method.getParameterTypes());
			loggableMethod = method.getAnnotation(Loggable.class);
		}

		// get current log level
		LogLevel logLevel = loggableMethod != null ? loggableMethod.value() : loggableClass.value();

		// show params
		boolean showParams = loggableMethod != null ? loggableMethod.params() : loggableClass.params();
		StringBuilder sb = new StringBuilder();
		sb.append("Start execution ");
		sb.append(method.getName());

		if (showParams && proceedingJoinPoint.getArgs() != null && proceedingJoinPoint.getArgs().length > 0) {
			sb.append('(');
			for (int i = 0; i < proceedingJoinPoint.getArgs().length; i++) {
				sb.append(method.getParameterTypes()[i].getName() + ":" + proceedingJoinPoint.getArgs()[i]);
				if (i < proceedingJoinPoint.getArgs().length - 1)
					sb.append(", ");
			}
			sb.append(')');
		} else {
			sb.append("()");
		}
		LogWriter.write(proceedingJoinPoint.getTarget().getClass(), logLevel, sb.toString());

		long startTime = System.currentTimeMillis();
		// start method execution
		Object result = proceedingJoinPoint.proceed();
		long endTime = System.currentTimeMillis();
		// show after
		sb = new StringBuilder();
		sb.append("Finished execution ");
		sb.append(method.getName());
		sb.append("() ");
		if (result != null) {
			boolean showResults = loggableMethod != null ? loggableMethod.result() : loggableClass.result();
			if (showResults) {
				sb.append(" result: ");
				sb.append(result);
				sb.append(' ');
			}
		}
		sb.append((endTime - startTime));
		sb.append(" millis time");
		LogWriter.write(proceedingJoinPoint.getTarget().getClass(), logLevel, sb.toString());

		return result;
	}
}