package ru.maksimov.andrey.commons.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.logging.LogLevel;

/**
 * Вспомогательный класс для записи логов
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class LogWriter {

	@SuppressWarnings("rawtypes")
	public static void write(Class clazz, LogLevel logLevel, String message) {
		Logger logger = LogManager.getLogger(clazz);

		switch (logLevel) {
		case TRACE:
			logger.trace(message);
			break;
		case DEBUG:
			logger.debug(message);
			break;
		case INFO:
			logger.info(message);
			break;
		case WARN:
			logger.warn(message);
			break;
		case ERROR:
			logger.error(message);
			break;
		case FATAL:
			logger.fatal(message);
			break;
		default:
			logger.warn("Unable suitable log level found");
			break;
		}
	}

}
