package ru.maksimov.andrey.commons.log;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Component;

/**
 * Аннотация для логирования
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
@Component
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface Loggable {
	LogLevel value() default LogLevel.INFO;

	boolean params() default true;

	boolean result() default true;
}