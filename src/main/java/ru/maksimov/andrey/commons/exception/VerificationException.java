package ru.maksimov.andrey.commons.exception;

/**
 * Исключение проверки
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public class VerificationException extends Exception {

	private static final long serialVersionUID = -2139091350156980855L;

	public VerificationException(String message) {
		super(message);
	}

	public VerificationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}